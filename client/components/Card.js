import React from 'react'
import AllOutIcon from '@material-ui/icons/AllOut';
import DoneIcon from '@material-ui/icons/Done';
import EcoIcon from '@material-ui/icons/Eco';
import LockIcon from '@material-ui/icons/Lock';

const Style = "text-white text-xs"

const arrayIcon = [<AllOutIcon fontSize="small" className={Style} />, <DoneIcon fontSize="small" className={Style} />, <EcoIcon fontSize="small" className={Style} />, <LockIcon fontSize="small" className={Style} />]
const color = ["from-transparent to-blue-600", "from-transparent to-green-600", "from-transparent to-red-600", "from-transparent to-yellow-500", "from-transparent to-purple-600"]

const Card = (props) => {
    return (
        <div className={`w-4/12 p-2 py-4 shadow-xl  border rounded-xl bg-gradient-to-r ${color[props.icon]} bg-opacity-20`} >
            <div className="flex justify-between">
                <div></div>
                <div className="flex items-center justify-center  bg-gray-300 rounded-xl m-1 p-2  bg-opacity-30 text-white text-2xl">
                    {props.value}
                </div>
            </div>
            <p className="text-gray-300  text-sm ">
                {props.growth ? 'Growth' : 'Total Traffic'}
            </p>
            <p className="text-gray-200  text-lg">
                {props.title}
            </p>
            <p className="text-gray-50 text-lg  font-semibold  ">
                {props.date}
            </p>






        </div>
    )
}

export default Card
