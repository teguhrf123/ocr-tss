import React, { useState } from 'react'
import { Line } from 'react-chartjs-2';
import { Upload, message,Table } from 'antd';

import { InboxOutlined } from '@ant-design/icons';
import RightBar from './RightBar';
const dummy = [{
    id: 0,
    dummyData: [200, 100, 420, 400, 1000, 250, 2000, 200, 100, 420, 400, 1000, 250, 2000, 200, 100, 420, 400, 1000],
    label: ['O6:00', 'O7:00', 'O8:00', 'O9:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00', 'O0:00']

},
{
    id: 1,
    dummyData: [2000, 90, 239, 401, 490, 100, 4029],
    label: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
},
{
    id: 2,
    dummyData: [400, 30, 420, 560],
    label: ['1st Week', '2nd Week', '3rd Week', '4th Week']
},
]
const dataSource = [
    {
      key: '1',
      count: 1,
      container_code: 'MW CX 500-620',
      timestamp: Date.now(),
    }
  ];
  
  const columns = [
    {
      title: 'Count',
      dataIndex: 'count',
      key: 'count',
    },
    {
      title: 'Container Code',
      dataIndex: 'container_code',
      key: 'container_code',
    },
    {
      title: 'Timestamp',
      dataIndex: 'timestamp',
      key: 'timestamp',
    },
  ];

const Middle = () => {
    const { Dragger } = Upload;
    const [dropdown, setDropdown] = useState(false)
    const [active, setActive] = useState(0)


    const data = {
        labels: dummy[active].label
        ,
        datasets: [
            {
                label: ['Traffic'],
                fill: false,
                lineTension: 0.1,
                backgroundColor: 'rgb(210, 210, 210)                ',
                borderColor: 'rgba(67, 56, 202)               ',
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: 'rgb(210, 210, 210)                ',
                pointBackgroundColor: '#000',
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'rgb(210, 210, 210)                ',
                pointHoverBorderColor: 'rgba(67, 56, 202)           ',
                pointHoverBorderWidth: 2,
                pointRadius: 4,
                pointHitRadius: 10,
                data: dummy[active].dummyData
            }
        ]
    };

    return (
        <div className=" bg-transparent ml-2   shadow-sm xl:w-full lg:w-full rounded-xl">
            <div className="flex space-x-3 items-center  ">
                <div class="relative inline-block text-left p-4 w-full">
                    <div>
                        <button onClick={() => setDropdown(!dropdown)} type="button" class="inline-flex rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-transparent text-sm font-medium text-gray-300 hover:bg-transparent focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-indigo-500" id="menu-button" aria-expanded="true" aria-haspopup="true">
                            {(active == 0 && 'Live Cam') || (active == 1 && 'Upload Video')}
                            <svg class="-mr-1 ml-2 h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                            </svg>
                        </button>
                    </div>
                    {
                        dropdown &&
                        <div class="absolute left-0 m-4 w-56 rounded-md shadow-lg bg-white ring-1 ring-white ring-opacity-5 outline-none" role="menu" aria-orientation="vertical" aria-labelledby="menu-button" tabindex="-1">
                            <div class="py-1" role="none">
                                <a onClick={() => {
                                    setActive(0)
                                    setDropdown(false)
                                }} href="#" class="text-gray-600 block px-4 py-2 text-sm" role="menuitem" tabindex="-1" id="menu-item-0">Live Cam</a>
                                <a onClick={() => {
                                    setActive(1)
                                    setDropdown(false)
                                }} href="#" class="text-gray-600 block px-4 py-2 text-sm" role="menuitem" tabindex="-1" id="menu-item-0">Upload Video</a>

                            </div>
                        </div>
                    }
                </div>
            </div>
            <div className="flex">
                <div className="flex flex-col items-center xl:w-full">
                    {
                        active == 0 ?
                            <div className="pt-4 mb-8">
                                <img src="/gif.gif" className=" xl:h-auto lg:h-auto px-4 w-full" />
                            </div> :
                            <div className="pt-4">
                                <Line data={data} />
                            </div>
                    }
                    <Table bordered dataSource={dataSource} columns={columns} pagination={false} style={{width: "100%", backgroundColor: "transparent", background: "transparent", padding: "30px"}}/>;

                </div>
                <RightBar/>
            </div>
        </div>
    )
}

export default Middle
