import React from 'react'
// import { Doughnut } from 'react-chartjs-2';

const data = {
    labels: [

    ],
    datasets: [{
        data: [10, 100],
        backgroundColor: [
            ' rgba(67, 56, 202)',
            'rgba(229, 231, 235)',

        ],
        hoverBackgroundColor: [
            '#FF6384',
            '#36A2EB',

        ]
    }]
};


const RightBar = () => {
    return (
        <div className="bg-white bg-opacity-5  w-6/12 rounded-xl flex flex-col items-center p-4">
            <img src='/container.png' className="mb-4" />
            <div className="w-full h-12 border flex items-center justify-center rounded-xl mb-4">
                <p className="text-2xl text-white font-bold mb-0">MW CX 500-620</p>
            </div>
            <div className="bg-white w-full bg-opacity-5 rounded-xl border border-gray-100">

                <div className="border-b p-3 border-gray-100">
                    <p className="font-semibold text-gray-300">Container Info</p>
                </div>
                <div className="flex flex-col  p-3 pb-0">
                    <p className="text-gray-300 text-sm">Timestamp : </p>
                </div>
                <div className="flex flex-col  p-3">
                    <p className="text-gray-300 text-sm">Container Size :</p>
                </div>
            </div>
            {/* <iframe width="auto" height="auto"
                src="https://www.youtube.com/embed/wqctLW0Hb_0" title="YouTube video player"
                frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen></iframe> */}
        </div>
    )
}

export default RightBar
