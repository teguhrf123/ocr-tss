import React, { useEffect, useState } from 'react'
import Card from './Card'
import Middle from './Middle'
import RightBar from './RightBar'
import ArrowUpIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownIcon from '@material-ui/icons/ArrowDownward';
import { io } from "socket.io-client";

const Container = () => {
    const sio = io('http://localhost:8000')
    const today = new Date(Date.now());
    const yesterday = new Date(Date.now() - 864e5);
    const [todayValue, setTodayValue] = useState(0)
    let yesterdayValue = 24
    const getPercentageChange = (oldNumber, newNumber) => {
        let decreaseValue = oldNumber - newNumber;

        return Math.abs((decreaseValue / oldNumber) * 100).toFixed(2);
    }
    sio.on('connect', () => {
        console.log("Connected")
    })
    sio.on('disconnect', () => {
        console.log("disconnected")
    })
    sio.on('count_data', (data) => {
        console.log(data, "PLER")
        setTodayValue(data)
    })
    return (
        <div className=" bg-gradient-to-r bg-dark h-auto " >
            {/* <div className="flex p-4 space-x-3">
                <Card title="TODAY" date={today.toDateString()} icon={4} value={todayValue} />
                <Card title="YESTERDAY" date={yesterday.toDateString()} icon={0} value={yesterdayValue} />
                <Card title="Percentage" date={(todayValue > yesterdayValue) ? getPercentageChange(yesterdayValue, todayValue) : `-${getPercentageChange(yesterdayValue, todayValue)}`} icon={(todayValue > yesterdayValue) ? 1 : 2} growth={true} value={(todayValue > yesterdayValue) ? <ArrowUpIcon /> : <ArrowDownIcon />} />
            </div> */}
            <div className="flex  ml-3 mt-6  mr-4">
                <Middle />
                {/* <RightBar /> */}
            </div>
        </div>
    )
}

export default Container
