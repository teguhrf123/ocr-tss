import { useContext, useState } from "react"
import Head from 'next/head'
import Image from 'next/image'
import SessionContext from "../helper/SessionContext"
import logo from '../public/logo.png'
export default function Login() {
    const { setUser } = useContext(SessionContext)
    const [name, setName] = useState('')
    const [password, setPassword] = useState('')
    const login = () => {
        if (name === 'ajis' && password === 'item') {
            setUser('ajis')
        }
        else if (name === '' || password === '') {
            alert('isi anjing')
        }
        else alert('Password salah tolol')
    }
    return (
        <div className="flex flex-col items-center justify-center min-h-screen py-2 bg-dark"
            >
            <Head>
                <title>Login</title>
                {/* <link rel="icon" href="/favicon.ico" /> */}
            </Head>
            <div className="w-1/3 h-96 bg-white bg-opacity-20 rounded-2xl flex flex-col justify-evenly items-center">
                <div className="flex flex-col justify-evenly items-center">
                    <div className="py-2 px-4">
                        <img src='/logo.png ' className="w-20 h-20" />
                    </div>
                    <h2 className="text-white text-xl text-left">Login To Your Account</h2>
                </div>
                <input className="outline-none border-b border-white bg-transparent placeholder-white py-2 font-bold text-white" placeholder="Username" onChange={(e) => setName(e.target.value)} />
                <input type="password" className="outline-none border-b border-white bg-transparent placeholder-white py-2 font-bold text-white" placeholder="Password" onChange={(e) => setPassword(e.target.value)} />
                <button className="bg-gradient-to-r from-dark-blue to-light-blue w-64 h-9 shadow-lg rounded-md text-white" onClick={login}>Login</button>
            </div>
            <div className=" flex flex-col">
                <a className="my-8 text-white font-bold text-center">Can't login?</a>
                <a className="text-white opacity-50 text-center">Privacy policy terms of use</a>
            </div>
        </div>
    )
}
