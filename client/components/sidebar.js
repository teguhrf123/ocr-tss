import React, { useContext } from 'react'
import DonutLargeIcon from '@material-ui/icons/DonutLarge';
import ClearAllIcon from '@material-ui/icons/ClearAll';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import SessionContext from '../helper/SessionContext';
import logo from '../public/logo.png'
import Image from 'next/image'

const Sidebar = () => {
  const { setUser } = useContext(SessionContext)
    return (
        <div className="md:w-3/12 w-6/12 bg-dark h-f">
            <div className=" py-4 flex justify-evenly items-center">
                <img src='/logo.png' className="w-16 h-16"/>
                <p className="xl:text-xl lg:text-md font-bold text-gray-200">DCT OCR (BETA)</p>
            </div>
            <div className="p-4 space-y-14">
                <div className="space-y-4" >
                    <h1 className="text-gray-300">Menu</h1>
                    <div className="">
                        <div className="flex p-3 text-gray-700  space-x-4 0 hover:bg-gray-50 rounded-xl cursor-pointer  ">
                            <DonutLargeIcon className=" text-gray-300" />
                            <p className="text-gray-300" >Dashboard</p>
                        </div>
                    </div>
                    <div className="">
                        <div className="flex p-3 text-gray-700  space-x-4 0 hover:bg-gray-50 cursor-pointer rounded-xl" onClick={() => setUser(null)}>
                            <ExitToAppIcon className="text-gray-300" />
                            <p className="text-gray-300  " >Logout</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    )
}

export default Sidebar
