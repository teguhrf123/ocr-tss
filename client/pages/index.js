import Head from 'next/head'
import Sidebar from '../components/sidebar'
import Header from "../components/Header"
import Container from '../components/Container'
import SessionContext from '../helper/SessionContext'
import { useContext, useEffect, useState } from 'react'
import Login from '../components/login'

export default function Home() {
  const { user } = useContext(SessionContext)
  
  return user ? (
    <div>
      <Head>
        <title>Home</title>
        {/* <link rel="icon" href="/favicon.ico" /> */}
      </Head>
      <div className="flex w-screen h-screen bg-dark" >
        <Sidebar />
        <div className="w-screen max-h-screen">
          {/* <Header /> */}
          <Container />
        </div>

      </div>

    </div>
  ) : (<Login />)
}
