// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
export default (req, res) => {
  res.statusCode = 200
  // stream = new Stream({
  //   name: 'name',
  //   streamUrl: 'rtsp://demo:demo@ipvmdemo.dyndns.org:5541/onvif-media/media.amp?profile=profile_1_h264&sessiontimeout=60&streamtype=unicast',
  //   wsPort: 9999,
  //   ffmpegOptions: { // options ffmpeg flags
  //     '-stats': '', // an option with no neccessary value uses a blank string
  //     '-r': 30 // options with required values specify the value after the key
  //   }
  // })
  res.json([{
    id: 0,
    location: "Bendungan Hilir",
    todayValue: '69',
    yesterdayValue: '420'
  },
  {
    id: 1,
    location: "Jl. Ahmad Yani",
    todayValue: '420',
    yesterdayValue: '69'
  },
  {
    id: 2,
    location: "Pekayon",
    todayValue: '1000',
    yesterdayValue: '300'
  },

])
}
