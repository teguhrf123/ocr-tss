import React, { createContext, useEffect, useState} from 'react'
import axios from 'axios'
const SessionContext = createContext({})

export const SessionProvider = (props) => {
    const [user, setUser] = useState(null)
    const [data, setData] = useState([])
    const [selectedData, setSelectedData] = useState({})
    
    useEffect(() => {
        axios.get('http://localhost:3000/api').then((res) => {
            setData(JSON.parse(JSON.stringify(res.data)))
        }).catch((e) => console.log(e))
    },[])
    return (
        <SessionContext.Provider value={{ user, setUser, data, setData, selectedData, setSelectedData }}>{props.children}</SessionContext.Provider>
    )
}

export default SessionContext
